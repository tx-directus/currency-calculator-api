const fx = require('money');

function getKeyFromCache(client, key) {

    var promise = new Promise(function(resolve, reject) {
        client.get(key, function(err, data) {
            if (err) {
                reject(err);
            }
            if (data === null) {
                reject({ message: 'no data' });
            } else {
                resolve(data);
            }
        });
    });

    return promise;
}


function doConvert(base, rates, from, to, amount) {
    // Apply exchange rates and base rate to `fx` library object:
    fx.rates = rates;
    fx.base = base;

    // money.js is ready to use:
    var result = fx.convert(amount, { from: from, to: to });
    return result.toFixed(2);

}

module.exports.getKeyFromCache = getKeyFromCache;
module.exports.doConvert = doConvert;