var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator/check');
const { sanitize } = require('express-validator/filter');
const oxr = require('open-exchange-rates');
const moment = require('moment');
const memjs = require('memjs');
const md5 = require('js-md5');
const { getKeyFromCache, doConvert } = require('../lib/utils');




router.get('/convert', [
    check('from').exists().withMessage('param from is required'),
    check('from').isAlpha().withMessage('param from must be a valid currency name'),
    check('from').isLength({ min: 3, max: 3 }).withMessage('param from must be 3 long'),

    check('to').exists().withMessage('to is required'),
    check('to').isAlpha().withMessage('param to must be a valid currency name'),
    check('to').isLength({ min: 3, max: 3 }).withMessage('param from must be 3 long'),

    check('amount').exists().withMessage('amount is required'),
    check('amount').isCurrency({ allow_negatives: false, digits_after_decimal: [1, 2] }).withMessage('param amount must be a valid currency number'),
    sanitize().trim()
], function(req, res, next) {

    var from = req.query.from.toUpperCase();
    var to = req.query.to.toUpperCase();
    var amount = req.query.amount;
    var datetime = moment().format('YYYY-MM-DD HH');
    var keyBase = md5(datetime + "_base").replace(/\s/g, '');
    var keyRates = md5(datetime + "_rates").replace(/\s/g, '');
    var promises = [];
    var responseObject = {
        response: {
            datetime: null,
            from: null,
            to: null,
            amount: 0,
            value: 0
        },
        errors: {
            relate: null,
            message: null
        }
    }
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        responseObject.errors.message = errors.mapped();
        return res.status(422).json(responseObject);
    }
    var client = memjs.Client.create(process.env.MEMCACHEDCLOUD_SERVERS, {
        username: process.env.MEMCACHEDCLOUD_USERNAME,
        password: process.env.MEMCACHEDCLOUD_PASSWORD
    });
    responseObject.response.datetime = moment();
    responseObject.response.from = from;
    responseObject.response.to = to;
    responseObject.response.amount = amount;
    responseObject.response.value = 0;
    promises.push(getKeyFromCache(client, keyRates));
    promises.push(getKeyFromCache(client, keyBase));


    Promise.all(promises).then(results => {

        var rates = JSON.parse(results[0]);
        var base = JSON.parse(results[1]);
        try {
            responseObject.response.value = doConvert(base, rates, from, to, amount);
        } catch (err) {
            console.log(err);
            responseObject.errors.message = err;
        }

        return res.json(responseObject);
    }).catch(noKeyerror => {
        oxr.set({
            app_id: process.env.ACCESS_KEY,
            base: from,
            symbols: [to]
        })
        oxr.latest(function() {
            responseObject.response.value = doConvert(oxr.base, oxr.rates, from, to, amount);
            client.set(keyBase, JSON.stringify(oxr.base), { expires: process.env.MEMCACHE_TIME }, function(err) {
                if (err) {
                    console.log(err.message);
                }
            });
            client.set(keyRates, JSON.stringify(oxr.rates), { expires: process.env.MEMCACHE_TIME }, function(err) {
                if (err) {
                    console.log(err.message);
                }
            });
            return res.json(responseObject);
        });
    });


});

module.exports = router;